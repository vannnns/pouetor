# pouetor

Pouetor is a friendly irc bot written in F#.  
It emits "pouet" shouts at everybody, with the count of **U** following a gaussian distribution

## publish for .netcore 2.1 self contained target linux-x64

`dotnet publish -c Release -r linux-x64 --self-contained true`

## copy to server

`scp -r linux-x64/ login@ip:~/server/pouetor`

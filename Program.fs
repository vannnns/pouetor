﻿open System
open Meebey.SmartIrc4net
open MathNet.Numerics.Distributions

let generateResponse (normal : Normal) : string =
    let uCount = Math.Max(0.0, normal.Sample() |> Math.Round) |> int
    let start = "\x0312,9\x02P" + "\x035O" + "\x038U"
    let uuuu = String.replicate uCount "\x038U"
    let ending = "\x0313E" + "\x0312T !!"
    start + uuuu + ending

let getResponse (normal : Normal) (m : string) : string option =
    if m = "!pouet" then
        Some ((generateResponse normal).ToString())
    else
        None

let handleMessage (irc : IrcClient) (chan : string) (normal: Normal) (e : IrcEventArgs) : unit =
    match getResponse normal e.Data.Message with
    | Some r -> irc.SendMessage(SendType.Message, chan, r, Priority.Medium)
    | _ -> ()

[<EntryPoint>]
let main argv =
    let server = "51.15.169.172"
    let chan = "#linux"
    let normal = Normal.WithMeanPrecision(2.0, 0.3)
    let irc = IrcClient()
    irc.OnChannelMessage.Add(handleMessage irc chan normal)
    irc.Connect(server, 6667)
    irc.Login("pouetor", "pouetor", 0, "pouetor")
    irc.RfcJoin chan
    irc.Listen true
    0